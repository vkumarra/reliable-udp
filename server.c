/* include udpserv1 */
#include "unpifiplus.h"
#include <string.h>
#include <signal.h>
#include "np1.h"

int count, clicount;

struct servsockinfo{
	int sockfd;
	int longestmatch;
	in_addr_t ipaddress;
	in_addr_t netmaskaddress;
	in_addr_t subnetaddress;
 	};

struct reqinprogress{
	u_short port;
	in_addr_t ipaddress;
	};

struct reqinprogress clientinfo[16];
 	
struct servsockinfo servinfo[16];

static void sig_alrm(int signo)
{
   return;                     /* just interrupt the recvfrom() */
}


static void sigchld_hdl (int sig)
{
	int k;
	pid_t pid;
	/* Wait for all dead processes.
	 * We use a non-blocking call to be sure this signal handler will not
	 * block if a child was cleaned up in another part of the program. */
	printf("CHILD %d terminated\n",pid);
	while ( (pid =waitpid(-1, NULL, WNOHANG)) > 0) {
		
		for (k=0; k<16; k++)
		{
				clientinfo[k].ipaddress = inet_addr("0.0.0.0");
				clientinfo[k].port = 0;
		}
	}
}


void mydg_echo(struct servsockinfo childsocket, char filename[], int wsize,struct sockaddr *pcliaddr, socklen_t clilen)
{
	int n, i = 0, localflag=0,len1, sockfd, sockfd1, resend = 0, pcounter;
	const int on = 1;
	char mesg[MAXLINE],rmesg[MAXLINE];
	char str[INET_ADDRSTRLEN];
	struct sockaddr_in myaddr,cliaddr,cliaddr1,servaddr;
	socklen_t	len;
	in_addr_t IPserver,IPclient,servsubnetaddress;

/* Get the socket IP address. */
	myaddr.sin_addr.s_addr = childsocket.ipaddress;
	len1 = sizeof(cliaddr);

/* Get the file name from the client. */

	inet_pton(AF_INET, Sock_ntop_host(pcliaddr, sizeof(*pcliaddr)), &(cliaddr.sin_addr));
	inet_ntop(AF_INET, &(cliaddr.sin_addr.s_addr), mesg, INET_ADDRSTRLEN);
	printf("\nclient IP Address is \t%s\n", mesg);
	IPclient=inet_addr(mesg);
/* Print the IP address of the server at which we got the request. */
	printf("\nServer Address is %s",  inet_ntoa(*(struct in_addr *)&childsocket.ipaddress));
	
	IPclient = inet_addr(mesg);

/* Check if the ipaddress is loopbackaddress */

	if (IPclient == inet_addr("127.0.0.1") ){
		IPserver= inet_addr("127.0.0.1");
		localflag = 1;
	}
/* Else check if the address belongs to local (extended network) */	
	else{
		for (i = 0; i < count; i++){
			servsubnetaddress = servinfo[i].netmaskaddress & IPclient;
			if ( servinfo[i].subnetaddress == servsubnetaddress ){
				localflag = 2;
				IPserver=servinfo[i].ipaddress;
			}
		}
	}
	if (localflag == 1)
		printf("\nclient address is loopback address");
	else if (localflag == 2)
		printf("\nclient address is local to the server");
	else
		printf("\nclient address is not local");

/* Bind a IPServer address and the Ephimeral port. */
	IPserver = childsocket.ipaddress;

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr= IPserver;	// change the ipaddress to struct addess
	servaddr.sin_port = htons(0);
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)); 
	len1 = sizeof(servaddr);

	if( (bind(sockfd, (SA *) &servaddr, len1)) == -1)
		{
			printf("\n server : Unable to bind socket : %d", sockfd);
			exit(0);
		}
// used to retrieve servers ip and port

	errno = getsockname(sockfd,(SA *)&servaddr,&len1);

	printf("\nServer Address and port are %s:%u",  inet_ntoa(servaddr.sin_addr),  (unsigned)ntohs(servaddr.sin_port));
	memset(rmesg, 0, sizeof(rmesg));
	sprintf(rmesg,"%u",(unsigned)ntohs(servaddr.sin_port)); 
	printf("\nServer ephemeral port number is %s",rmesg);

	len1 = sizeof(cliaddr1);
	
	if (localflag == 1 | localflag == 2)
		setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR| SO_DONTROUTE, &on, sizeof(on));
	else
		setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)); 

	printf("\nclient port is %d", ((struct sockaddr_in*)pcliaddr)->sin_port);

/* Connect to the client's IP address and the ephemeral port. */

	if ( connect(sockfd, pcliaddr, clilen) < 0 ){
		perror("\nconnection error.");
		printf("\nCould not Connect to client");
		exit(0);
	}
	errno = getpeername(sockfd,(SA *)&cliaddr1,&len1);

	printf("\nClient Address and port are %s:%u\n\n",  inet_ntoa(cliaddr1.sin_addr),  (unsigned)ntohs(cliaddr1.sin_port));
signal(SIGPIPE, SIG_IGN);
signal(SIGALRM, sig_alrm);
	pcounter = 0;
sendport:
	errno = sendto(childsocket.sockfd, rmesg, sizeof(rmesg), 0, pcliaddr, clilen);
//	if (resend == 1)
//		sendto(sockfd, mesg, sizeof(mesg), 0, pcliaddr, clilen);
alarm(2);
if( (n = recvfrom(sockfd, mesg, MAXLINE, 0, NULL, NULL) ) <0 ){
	 if (errno == EINTR){
		pcounter++;
                printf("Packet lost. so resending the port number\n");
		//write(1,buff,sizeof(buff));
		resend = 1;
		if (pcounter > 11 )
		{
			printf("No response from client. server giving up");
			exit(0);
		}
		goto sendport;}
            else{
                printf("recv error\n");
		}
        } else {
            alarm(0);}
/*
   should wait for the acknowledgment, if not recieved it should send the data (port number) using childsocket.sockfd and sockfd
   Once the ack is recieved, close the listning socket (childsocket) 
*/
close(childsocket.sockfd);

	dg_send_file(sockfd, pcliaddr, clilen, filename,wsize);
/*	for ( ; ; ) 
	{
		n = recvfrom(sockfd, mesg, MAXLINE, 0, NULL, NULL);
		printf("Rcvd %d chars",n);
		//printf("child %d, datagram from %s", getpid(), Sock_ntop(pcliaddr, len));
		//printf(", to %s\n", Sock_ntop(myaddr, len));
		sendto(sockfd, mesg, n, 0, NULL, 0);
	}
*/
close(sockfd);
}

int main(int argc, char **argv)
{
	int sockfd, activefd, port, wsize,n,i,j,k,maxfd = 0; 
	char line1[MAXLINE],line2[MAXLINE],buff[MAXLINE],filename[128];
	fd_set setallfd; 
	const int on = 1;
	pid_t pid;
	struct sockaddr_in *sa;
	struct sockaddr *sa1;
	struct ifi_info	*ifi, *ifihead;
	struct sockaddr_in cliaddr;
	struct sigaction act;
	socklen_t clilen;
	FILE *fp;
	
/* Read and display data from server.in file */
	fp = fopen ("server.in", "r");

	if (fp == NULL)
	{
		printf("\ncouldn't open the file server.in");
		exit(0);
	}

	fgets(line1, MAXLINE, fp);
	port = atoi(line1);

	fgets(line2,MAXLINE,fp);
	wsize = atoi(line2);
	fclose(fp); 
	
	printf("\nserver.in file parameters:-\n");
	printf("\nServer Port : %d", port);
	printf("\nWindow Size : %d\n", wsize);
	
	for (i=0; i<16; i++){
		clientinfo[i].port = 0;
		clientinfo[i].ipaddress = inet_addr("0.0.0.0");
	}
/* Iterating through all the interfaces. */
	for (ifihead = ifi = Get_ifi_info_plus(AF_INET, 1);
		 ifi != NULL; ifi = ifi->ifi_next) 
	{

		sockfd = socket(AF_INET, SOCK_DGRAM, 0);

		setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)); 

		sa = (struct sockaddr_in *) ifi->ifi_addr;
		sa->sin_family = AF_INET;
		sa->sin_port = htons(port);
		
		if( (bind(sockfd, (SA *) sa, sizeof(*sa))) == -1)
		{
			perror("\n server : Unable to bind socket.");
			exit(0);
		}
		
		servinfo[count].sockfd = sockfd;
		servinfo[count].ipaddress = sa->sin_addr.s_addr;
		if ( (sa1 = ifi->ifi_addr) != NULL)
			printf("  IP address: %s\n",
						Sock_ntop_host(sa1, sizeof(*sa1)));
		
		sa = (struct sockaddr_in *) ifi->ifi_ntmaddr;
		servinfo[count].netmaskaddress = sa->sin_addr.s_addr;

		if ( (sa1 = ifi->ifi_ntmaddr) != NULL)
			printf("  network mask: %s\n",
						Sock_ntop_host(sa1, sizeof(*sa1)));
		
		servinfo[count].subnetaddress = servinfo[count].netmaskaddress & servinfo[count].ipaddress;
		printf("  subnet Address: %s\n\n",  inet_ntoa(*(struct in_addr *)&servinfo[count].subnetaddress));
		
		count++;
	}
	

	printf("\n Number of interfaces: %d\n", count);
	count--;
	
	memset (&act, 0, sizeof(act));
	act.sa_handler = sigchld_hdl;
	if (sigaction(SIGCHLD, &act, 0)){
		perror("sigaction");
		return 1;
	}
	
	maxfd = servinfo[0].sockfd;
	for(i=0; i<=count ; i++){
		if(maxfd < servinfo[i].sockfd)
			maxfd = servinfo[i].sockfd;
	}

/* Select is used to handle binds on different interfaces */
selectloop:
	while(1)
	{	
		FD_ZERO(&setallfd);
		for(i=0; i<=count ; i++)
		{
			FD_SET(servinfo[i].sockfd, &setallfd);
		}
		printf("\nServer : Waiting at select \n");
		n = select(maxfd+1, &setallfd, NULL, NULL, NULL);
		
		if(n == -1)
		{
			// printf("\nServer :Select Error occured. \n");
			goto selectloop;
		}
		
		for(i=0; i<=count ; i++)
		{
			if(FD_ISSET(servinfo[i].sockfd, &setallfd))
			{
				activefd = servinfo[i].sockfd;
				clilen = sizeof(cliaddr);
                memset(filename, 0, sizeof(filename));
                n = recvfrom(activefd, filename, MAXLINE, 0, (struct sockaddr *) &cliaddr, &clilen);
		for (k=0; k<16; k++)
		{
			if (clientinfo[k].port == cliaddr.sin_port && clientinfo[k].ipaddress == cliaddr.sin_addr.s_addr){
				printf("\nduplicate request came... Ignoring the request");
				goto selectloop;
			}
		}
		clientinfo[clicount].port = cliaddr.sin_port;
		clientinfo[clicount++].ipaddress = cliaddr.sin_addr.s_addr;
		
				if (n < 0 )
				{
					printf("error in recvfrom : %d " , n);
					exit(0);
				}
// should send and recieve an ack
				printf("\nThe file to be transferred is %s\n", filename);

				printf("\nServer : Forking a child. ");
				if ( (pid = fork()) == 0){
					for (j = 0; j < count; j++){
						if(activefd != servinfo[j].sockfd)
						close(servinfo[j].sockfd);
					}
					mydg_echo(servinfo[i], filename,wsize, (struct sockaddr *) &cliaddr, sizeof(cliaddr) );
					exit(0);
				}
				else if (pid <0 ){
					perror("\nfork Error!!!!");
			//		exit(0);
				}
			}
		}
	}
}


