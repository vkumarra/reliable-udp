#ifndef __NP_H
#define __NP_H

#define SEGMENT_DATA_SIZE 496
// #define MAXLINE 256
#define RTT_RXTMIN 1000000
#define RTT_RXTMAX 3000000
#define RTT_MAXNREXMT 12
#define ACK_MASK 0x0001
#define FIN_MASK 0x0002
#define FIN_SIZE_MASK 0x0FFC
#define IS_FIN(A) (A & FIN_MASK)>>1
#define IS_ACK(A) (A & ACK_MASK)
#define FIN_SIZE(A) (A & FIN_SIZE_MASK)>>2
#define SET_FIN(A) (A | FIN_MASK)
#define SET_ACK(A) (A | ACK_MASK)
#define SA struct sockaddr
//#define RTT_RTOCALC(ptr) ((ptr)->rtt_srtt + (4.0 * (ptr)->rtt_rttvar))

/* Packet header structure */
struct hdr {
    uint32_t seq; // sequence number 
	uint32_t ts;  // # timestamp when packet was sent
	uint16_t win_size;  // window size
    uint8_t ack;
    uint8_t fin;
    // bit 0: ACK flag, bit 1: FIN flag, bit 2-11: size of segment in case bit 1 is set
    uint16_t seg_size;
};

/* Node structure for circular list buffering packets */
struct node {
        char data[SEGMENT_DATA_SIZE];
	struct hdr header;
        struct node *next;
        struct node *prev;
	int ack;
	int seq_no;
	int recv_flag;
    int fin;
    int seg_size;
	int ack_count;
};

struct send_buf_indx {
	struct node *unack_buf_ptr;
	struct node *last_sent_ptr;
};

struct recv_buf_indx {
	struct node *expt_seg_ptr;
	struct node *unread_buf_ptr;
};

struct thread_param {
    int time_val;
};

/*
struct recv_buf_indx {


};
*/

/* The formulae
	delta = rtt_rtt - rtt_srtt
	srtt = rtt_srtt + g*delta
	rtt_rttvar = rtt_rttvar + h*(delta - rtt_rttvar)
	rtt_rto = rtt_srtt + 4*rtt_rttvar
	
	double rtt_rto every time timeout occurs on a given segment
	when a transmitted segment is ACKed use the latest RTO value for next retransmission, this causes RTOs to be reset
	g = 1/8 and h = 1/4
	
*/

struct rtt_info {
  uint32_t    rtt_rtt;        /* most recent measured RTT, in seconds, (measuredRTT in doc) */
  uint32_t    rtt_srtt;       /* smoothed RTT estimator, in seconds, (srtt in doc) */
  uint32_t    rtt_rttvar;     /* smoothed mean deviation, in seconds (rttvar in the doc) */
  uint32_t    rtt_rto;        /* current RTO to use, in seconds */
  uint32_t    rtt_nrexmt;     /* # times retransmitted: 0, 1, 2, ... */
  uint32_t    rtt_base;       /* # sec since 1/1/1970 at start */
};

/*Reads the client inputs */
 	

int
rtt_minmax(int rto)
{
        if (rto < RTT_RXTMIN)
                rto = RTT_RXTMIN;
        else if (rto > RTT_RXTMAX)
                rto = RTT_RXTMAX;
        return(rto);
}

int rtt_rtocalc(struct rtt_info *ptr) {
	
	return (ptr->rtt_rttvar + (ptr->rtt_srtt)>>3);
}

void
rtt_init(struct rtt_info *ptr)
{
        struct timeval  tv;

        if (gettimeofday(&tv, NULL) == -1) {
		perror("gettimeofday error: ");
	}

        ptr->rtt_base = tv.tv_sec*1000000 + tv.tv_usec;              /* #  micro seconds since 1/1/1970 at start */

        ptr->rtt_rtt    = 0;
        ptr->rtt_srtt   = 0;
        ptr->rtt_rttvar = 250000; // need to figure out what will be the initial value of time out when system starts
	ptr->rtt_rto = rtt_rtocalc(ptr);
	ptr->rtt_rto = rtt_minmax(ptr->rtt_rto);
                /* first RTO at (srtt + (4 * rttvar)) = 1 second */
}

uint32_t
rtt_ts(struct rtt_info *ptr)
{
        uint32_t                ts;
        struct timeval  tv;

        if (gettimeofday(&tv, NULL) == -1) {
		perror("gettimeofday error: ");
	}

	ts = (tv.tv_sec*1000000 + tv.tv_usec) - ptr->rtt_base;
        return(ts);
}

void
rtt_newpack(struct rtt_info *ptr)
{
        ptr->rtt_nrexmt = 0;
}

void rtt_packwatch(struct rtt_info *ptr) 
{
        ptr->rtt_nrexmt = 1;
}

/* need more clarity here, its called to set the alarm when a packet is sent out, why is .5 seconds being added here */
int
rtt_start(struct rtt_info *ptr)
{
	ptr->rtt_rto = ptr->rtt_rto/1000000;
        return((int) (ptr->rtt_rto + 0.5));             /* round float to int */
                /* 4return value can be used as: alarm(rtt_start(&foo)) */
}

int
rtt_timeout(struct rtt_info *ptr)
{
        ptr->rtt_rto *= 2;              /* next RTO */
	ptr->rtt_rto = rtt_minmax(ptr->rtt_rto);

        if (++ptr->rtt_nrexmt > RTT_MAXNREXMT)
                return(-1);                     /* time to give up for this packet */
        return(0);
}

void
rtt_stop(struct rtt_info *ptr, uint32_t ms)
{
	ptr->rtt_rtt = ms;  // measured rtt in microseconds
	ptr->rtt_rtt -= (ptr->rtt_srtt)>>3;
	ptr->rtt_srtt += ptr->rtt_rtt; // updated value of srtt at 8 times its value
	
	if (ptr->rtt_rtt < 0) {
		ptr->rtt_rtt = -ptr->rtt_rtt;
	}

	ptr->rtt_rtt -= (ptr->rtt_rttvar)>>2;
	ptr->rtt_rttvar += ptr->rtt_rtt; // updated value of rttvar at 4 times its value

	ptr->rtt_rto = (ptr->rtt_srtt)>>3 + ptr->rtt_rttvar;
	ptr->rtt_rto = rtt_minmax(ptr->rtt_rto);	
}

double random_gen(int seed)
{
	struct timeval  tv;

        if (gettimeofday(&tv, NULL) == -1) {
                perror("gettimeofday error: ");
        }

        srand48(tv.tv_sec + tv.tv_usec);
        return drand48();
}

struct node *create_list(int window_size)
{
        int i=0;
        struct node *start = (struct node *)malloc(sizeof(struct node));
        start->prev = NULL;
        struct node *temp = start, *temp1 = start;

        while (i < window_size-1) {
                struct node *n = (struct node *)malloc(sizeof(struct node));
		memset(n, 0, sizeof(struct node));
                temp->next = n;
                n->prev = temp;
                temp = n;
		i++;
        }

        temp1->prev = temp;
        temp->next = temp1;
	return start;
}

int free_list(int window_size, struct node *start) 
{
	struct node *temp = start->prev, *temp1;
	temp->next = NULL;
	start->prev = NULL;
	
	temp = start;
	while(temp != NULL) {
		temp1 = temp->next;
		free(temp);
		temp = temp1;
	}
}

void *handle_ack(void *arg);
int send_ack(int sockfd, struct hdr *header);
void *reader(void *arg);
int window_size();
#endif
